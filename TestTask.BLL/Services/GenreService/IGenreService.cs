﻿using System.Collections.Generic;
using TestTask.BLL.Services.GenreService.Dtos;

namespace TestTask.BLL.Services.GenreService
{
    public interface IGenreService
    {
        int GetGenresTotalCount();
        List<GenreDto> GetGenres(int skip, int take);
        List<GenreWithoutBooksDto> GetGenresWithoutBooks(int skip, int take, string name);
        GenreDto GetGenreById(int genreId);
        GenresWithPageDto GetGenresWithPageDto(int? pageNumber, int? itemsCount);
    }
}