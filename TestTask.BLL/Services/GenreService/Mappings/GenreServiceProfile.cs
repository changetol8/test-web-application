﻿using AutoMapper;
using TestTask.BLL.Services.AuthorService.Dtos;
using TestTask.BLL.Services.BookService.Dtos;
using TestTask.BLL.Services.GenreService.Dtos;
using TestTask.DAL.Entities;

namespace TestTask.BLL.Services.GenreService.Mappings
{
    public class GenreServiceProfile : Profile
    {
        public GenreServiceProfile()
        {
            CreateMap<Author, AuthorDto>();
            CreateMap<Book, BookDto>();
            CreateMap<Genre, GenreDto>();
            CreateMap<GenreWithoutBooksDto, Genre>().ReverseMap();
        }
    }
}