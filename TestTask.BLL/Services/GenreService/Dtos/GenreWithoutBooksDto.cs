﻿namespace TestTask.BLL.Services.GenreService.Dtos
{
    public class GenreWithoutBooksDto
    {
        public int GenreId { get; set; }
        public string Name { get; set; }
    }
}