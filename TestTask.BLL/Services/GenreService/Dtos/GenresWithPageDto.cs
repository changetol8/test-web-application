﻿using System.Collections.Generic;
using TestTask.BLL.Services.PagingService.Dtos;

namespace TestTask.BLL.Services.GenreService.Dtos
{
    public class GenresWithPageDto
    {
        public List<GenreDto> Genres { get; set; }
        public PageInfoDto PageInfo { get; set; }
    }
}