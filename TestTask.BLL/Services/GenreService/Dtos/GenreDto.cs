﻿using System.Collections.Generic;
using TestTask.BLL.Services.BookService.Dtos;

namespace TestTask.BLL.Services.GenreService.Dtos
{
    public class GenreDto
    {
        public int GenreId { get; set; }
        public string Name { get; set; }
        public List<BookDto> Books { get; set; }
    }
}