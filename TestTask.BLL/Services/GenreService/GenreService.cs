﻿using AutoMapper;
using System.Collections.Generic;
using TestTask.BLL.Services.GenreService.Dtos;
using TestTask.BLL.Services.PagingService;
using TestTask.DAL.Interfaces;
using TestTask.DAL.Repositories.Queries;

namespace TestTask.BLL.Services.GenreService
{
    public class GenreService : IGenreService
    {
        private readonly IGenreRepository _genreRepository;
        private readonly IPagingService _pagingService;
        private readonly IMapper _mapper;

        public GenreService(IGenreRepository genreRepository,
                            IPagingService pagingService,
                            IMapper mapper)
        {
            _genreRepository = genreRepository;
            _pagingService = pagingService;
            _mapper = mapper;
        }

        public int GetGenresTotalCount()
        {
            var genresCount = _genreRepository.GetGenresTotalCount();

            return genresCount;
        }

        public List<GenreDto> GetGenres(int skip, int take)
        {
            var genres = _genreRepository.GetGenres(skip, take);
            var genreDtos = _mapper.Map<List<GenreDto>>(genres);

            return genreDtos;
        }

        public List<GenreWithoutBooksDto> GetGenresWithoutBooks(int skip, int take, string name)
        {
            var query = new GetGenresQuery
            {
                GenreName = name
            };

            var genres = _genreRepository.GetGenres(skip, take, query);
            var genresWithoutBooks = _mapper.Map<List<GenreWithoutBooksDto>>(genres);

            return genresWithoutBooks;
        }

        public GenreDto GetGenreById(int genreId)
        {
            var genre = _genreRepository.GetGenreById(genreId);
            var genreDto = _mapper.Map<GenreDto>(genre);

            return genreDto;
        }

        public GenresWithPageDto GetGenresWithPageDto(int? pageNumber, int? itemsCount)
        {
            var genresTotalCount = GetGenresTotalCount();
            var page = _pagingService.GetPageInfo(genresTotalCount, pageNumber, itemsCount);

            var genreDtos = GetGenres(page.CurrentPage, page.ItemsCountPerPage);

            var genresWithPageDto = new GenresWithPageDto
            {
                Genres = genreDtos,
                PageInfo = page,
            };

            return genresWithPageDto;
        }
    }
}