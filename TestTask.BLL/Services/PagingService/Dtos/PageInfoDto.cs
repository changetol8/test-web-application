﻿namespace TestTask.BLL.Services.PagingService.Dtos
{
    public class PageInfoDto 
    {
        public int PageNumber { get; set; }
        public int ItemsCountPerPage { get; set; }
        public int TotalItemsCount { get; set; }
        public int PagesCount { get; set; } 
        public int CurrentPage { get; set; }
    }
}