﻿using TestTask.BLL.Services.PagingService.Dtos;

namespace TestTask.BLL.Services.PagingService
{
    public class PagingService : IPagingService
    {
        public PageInfoDto GetPageInfo(int totalItemsCount, int? pageNumber, int? itemsCountPerPage)
        {
            var pagesCount = totalItemsCount / (itemsCountPerPage ?? 10);
            var itemsCount = itemsCountPerPage ?? 10;
            var number = pageNumber ?? 1;
            var currentPage = (number - 1) * itemsCount;

            var pageInfo = new PageInfoDto
            {
                TotalItemsCount = totalItemsCount,
                ItemsCountPerPage = itemsCount,
                CurrentPage = currentPage,
                PagesCount = pagesCount,
                PageNumber = number,
            };

            return pageInfo;
        }
    }
}