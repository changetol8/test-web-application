﻿using TestTask.BLL.Services.PagingService.Dtos;

namespace TestTask.BLL.Services.PagingService
{
    public interface IPagingService
    {
        PageInfoDto GetPageInfo(int totalItemsCount, int? pageNumber, int? itemsCountPerPage);
    }
}