﻿using System.Collections.Generic;
using TestTask.BLL.Services.AuthorService.Dtos;

namespace TestTask.BLL.Services.AuthorService
{
    public interface IAuthorService 
    {
        int GetAuthorsTotalCount();
        List<AuthorDto> GetAuthors(int skip, int take);
        List<AuthorWithoutBooksDto> GetAuthorsWithoutBooks(int skip, int take, string name);
        AuthorDto GetAuthorById(int authorId);
        AuthorsWithPageDto GetAuthorsWithPageDto(int? pageNumber, int? itemsCount);
    }
}