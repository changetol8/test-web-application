﻿using AutoMapper;
using TestTask.BLL.Services.AuthorService.Dtos;
using TestTask.BLL.Services.BookService.Dtos;
using TestTask.BLL.Services.GenreService.Dtos;
using TestTask.DAL.Entities;

namespace TestTask.BLL.Services.AuthorService.Mappings
{
    public class AuthorServiceProfile : Profile
    {
        public AuthorServiceProfile()
        {
            CreateMap<Book, BookDto>();
            CreateMap<Genre, GenreDto>();
            CreateMap<Author, AuthorDto>();
            CreateMap<Author, AuthorWithoutBooksDto>().ReverseMap();
        }
    }
}