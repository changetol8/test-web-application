﻿using System;
using System.Collections.Generic;
using TestTask.BLL.Services.BookService.Dtos;

namespace TestTask.BLL.Services.AuthorService.Dtos
{
    public class AuthorDto 
    {
        public int AuthorId { get; set; }
        public string Surname { get; set; }
        public string Name { get; set; }
        public string Patronymic { get; set; }
        public DateTime? BirthDate { get; set; }
        public List<BookDto> Books { get; set; }
    }
}