﻿using System.Collections.Generic;
using TestTask.BLL.Services.PagingService.Dtos;

namespace TestTask.BLL.Services.AuthorService.Dtos
{
    public class AuthorsWithPageDto 
    {
        public List<AuthorDto> Authors { get; set; }
        public PageInfoDto PageInfo { get; set; }
    }
}