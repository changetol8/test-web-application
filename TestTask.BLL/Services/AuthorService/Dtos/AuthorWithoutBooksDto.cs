﻿using System;

namespace TestTask.BLL.Services.AuthorService.Dtos
{
    public class AuthorWithoutBooksDto
    {
        public int AuthorId { get; set; }
        public string Surname { get; set; }
        public string Name { get; set; }
        public string Patronymic { get; set; }
        public DateTime? BirthDate { get; set; }
    }
}