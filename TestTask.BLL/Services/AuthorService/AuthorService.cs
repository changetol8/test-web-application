﻿using AutoMapper;
using System.Collections.Generic;
using TestTask.BLL.Services.AuthorService.Dtos;
using TestTask.BLL.Services.PagingService;
using TestTask.DAL.Interfaces;
using TestTask.DAL.Repositories.Queries;

namespace TestTask.BLL.Services.AuthorService
{
    public class AuthorService : IAuthorService
    {
        private readonly IAuthorRepository _authorRepository;
        private readonly IPagingService _pagingService;
        private readonly IMapper _mapper;

        public AuthorService(IAuthorRepository authorRepository, 
                             IPagingService pagingService, 
                             IMapper mapper)
        {
            _authorRepository = authorRepository;
            _pagingService = pagingService;
            _mapper = mapper;
        }

        public int GetAuthorsTotalCount()
        {
            var authorsCount = _authorRepository.GetAuthorsTotalCount();

            return authorsCount;
        }

        public List<AuthorWithoutBooksDto> GetAuthorsWithoutBooks(int skip, int take, string name)
        {
            var query = new GetAuthorsQuery
            {
                AuthorName = name
            };

            var authors = _authorRepository.GetAuthors(skip, take, query);
            var authorsWithoutBooksDto = _mapper.Map<List<AuthorWithoutBooksDto>>(authors);

            return authorsWithoutBooksDto;
        }

        public List<AuthorDto> GetAuthors(int skip, int take)
        {
            var authors = _authorRepository.GetAuthors(skip, take);
            var authorDtos = _mapper.Map<List<AuthorDto>>(authors);

            return authorDtos;
        }

        public AuthorDto GetAuthorById(int authorId)
        {
            var author = _authorRepository.GetAuthorById(authorId);
            var authorDto = _mapper.Map<AuthorDto>(author);

            return authorDto;
        }

        public AuthorsWithPageDto GetAuthorsWithPageDto(int? pageNumber, int? itemsCount)
        {
            var authorsTotalCount = GetAuthorsTotalCount();
            var page = _pagingService.GetPageInfo(authorsTotalCount, pageNumber, itemsCount);

            var authorDtos = GetAuthors(page.CurrentPage, page.ItemsCountPerPage);

            var authorsWithPageDto = new AuthorsWithPageDto
            {
                Authors = authorDtos,
                PageInfo = page,
            };

            return authorsWithPageDto;
        }
    }
}