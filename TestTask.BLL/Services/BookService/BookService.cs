﻿using AutoMapper;
using FluentValidation;
using System.Collections.Generic;
using TestTask.BLL.Services.BookService.Dtos;
using TestTask.BLL.Services.BookService.Services;
using TestTask.BLL.Services.BookService.Validation;
using TestTask.BLL.Services.PagingService;
using TestTask.DAL.Entities;
using TestTask.DAL.Interfaces;
using TestTask.DAL.Repositories.Queries;

namespace TestTask.BLL.Services.BookService
{
    public class BookService : IBookService
    {
        private readonly ITextStatisticsService _textStatisticsService;
        private readonly IBookRepository _bookRepository;
        private readonly IPagingService _pagingService;
        private readonly IMapper _mapper;

        public BookService(ITextStatisticsService textStatisticsService,
                           IBookRepository bookRepository,
                           IPagingService pagingService,
                           IMapper mapper)
        {
            _textStatisticsService = textStatisticsService;
            _bookRepository = bookRepository;
            _pagingService = pagingService;
            _mapper = mapper;
        }

        public void UpdateBook(BookDto bookDto)
        {
            new BookValidator().ValidateAndThrow(bookDto);

            var bookForUpdate = _mapper.Map<Book>(bookDto);
            _bookRepository.UpdateBook(bookForUpdate);
        }

        public BookDto GetBook(int bookId)
        {
            var book = _bookRepository.GetBookById(bookId);
            var bookDto = _mapper.Map<BookDto>(book);

            var text = bookDto.Text;

            bookDto.BookStatistics = new BookStatisticsDto
            {
                BookTextLength = _textStatisticsService.TextLength(text),
                BookAverageSentenceLength = _textStatisticsService.AverageSentenceLength(text),
                BookUniqueWordsCount = _textStatisticsService.UniqueWordsCount(text),
                BookAverageWordLength = _textStatisticsService.AverageWordLength(text),
                BookWordsCount = _textStatisticsService.WordsCount(text)
            };

            return bookDto;
        }

        public void CreateBook(BookDto bookDto)
        {
            new BookValidator().ValidateAndThrow(bookDto);

            var book = _mapper.Map<Book>(bookDto);
            _bookRepository.CreateBook(book);
        }

        public List<BookDto> GetBooks(int pageIndex, int itemsCount, SearchBookDto search)
        {
            var searchBookQuery = _mapper.Map<SearchBookQuery>(search);
            var books = _bookRepository.GetBooks(pageIndex, itemsCount, searchBookQuery);
            var bookDtos = _mapper.Map<List<BookDto>>(books);

            return bookDtos;
        }

        public int GetBooksTotalCount()
        {
            var booksCount = _bookRepository.GetBooksTotalCount();

            return booksCount;
        }

        public BooksWithPageDto GetBooksWithPageDto(int? pageNumber, int? itemsCount, SearchBookDto search = null)
        {
            var booksTotalCount = GetBooksTotalCount();
            var page = _pagingService.GetPageInfo(booksTotalCount, pageNumber, itemsCount);

            var bookDtos = GetBooks(page.CurrentPage, page.ItemsCountPerPage, search);

            var booksWithPageDto = new BooksWithPageDto
            {
                SearchBook = search,
                Books = bookDtos,
                PageInfo = page,
            };

            return booksWithPageDto;
        }
    }
}