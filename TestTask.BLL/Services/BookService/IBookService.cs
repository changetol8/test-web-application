﻿using System.Collections.Generic;
using TestTask.BLL.Services.BookService.Dtos;

namespace TestTask.BLL.Services.BookService
{
    public interface IBookService
    {
        void UpdateBook(BookDto bookDto);
        BookDto GetBook(int bookId);
        void CreateBook(BookDto bookDto);
        List<BookDto> GetBooks(int pageIndex, int itemsCount, SearchBookDto search = null);
        int GetBooksTotalCount();
        BooksWithPageDto GetBooksWithPageDto(int? pageNumber, int? itemsCount, SearchBookDto search = null);
    }
}