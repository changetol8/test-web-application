﻿namespace TestTask.BLL.Services.BookService.Dtos
{
    public class BookStatisticsDto
    {
        public int BookTextLength { get; set; }
        public int BookWordsCount { get; set; }
        public int BookUniqueWordsCount { get; set; }
        public int BookAverageWordLength { get; set; }
        public int BookAverageSentenceLength { get; set; }
    }
}