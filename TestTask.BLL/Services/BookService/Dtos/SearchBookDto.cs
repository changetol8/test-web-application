﻿using System;

namespace TestTask.BLL.Services.BookService.Dtos
{
    public class SearchBookDto
    {
        public string BookName { get; set; }
        public string AuthorName { get; set; }
        public string GenreName { get; set; }
        public DateTime? CreationDate { get; set; }
    }
}