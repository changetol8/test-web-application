﻿using System;
using System.Collections.Generic;
using TestTask.BLL.Services.AuthorService.Dtos;
using TestTask.BLL.Services.GenreService.Dtos;

namespace TestTask.BLL.Services.BookService.Dtos
{
    public class BookDto
    {
        public int BookId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime? CreationDate { get; set; }
        public string Isbn { get; set; }
        public string Text { get; set; }
        public BookStatisticsDto BookStatistics { get; set; }
        public List<AuthorWithoutBooksDto> Authors { get; set; }
        public List<GenreWithoutBooksDto> Genres { get; set; }
    }
}