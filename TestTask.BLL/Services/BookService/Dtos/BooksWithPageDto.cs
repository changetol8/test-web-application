﻿using System.Collections.Generic;
using TestTask.BLL.Services.PagingService.Dtos;

namespace TestTask.BLL.Services.BookService.Dtos
{
    public class BooksWithPageDto
    {
        public List<BookDto> Books { get; set; }
        public SearchBookDto SearchBook { get; set; }
        public PageInfoDto PageInfo { get; set; }
    }
}