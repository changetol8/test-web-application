﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace TestTask.BLL.Services.BookService.Services
{
    public class TextStatisticsService : ITextStatisticsService
    {
        public int TextLength(string text)
        {
            if (string.IsNullOrEmpty(text))
            {
                return 0;
            }

            var trimmedText = text.Trim();
            var textLength = trimmedText.Length;

            return textLength;
        }

        public int WordsCount(string text)
        {
            var words = SplitByWords(text);

            if (words == null)
            {
                return 0;
            }

            var wordsCount = words.Length;

            return wordsCount;
        }

        public int UniqueWordsCount(string text)
        {
            var words = SplitByWords(text);

            if (words == null)
            {
                return 0;
            }

            var uniqueWords = new HashSet<string>(words, StringComparer.OrdinalIgnoreCase);
            var uniqueWordsCount = uniqueWords.Count;

            return uniqueWordsCount;
        }

        public int AverageWordLength(string text)
        {
            if (string.IsNullOrEmpty(text))
            {
                return 0;
            }

            var wordsCount = WordsCount(text);

            if (wordsCount == 0)
            {
                return 0;
            }

            var words = SplitByWords(text);
            var wordsLengthSum = words.Sum(x => x.Length);
            var averageWordLength = wordsLengthSum / wordsCount;

            return averageWordLength;
        }

        public int AverageSentenceLength(string text)
        {
            if (string.IsNullOrWhiteSpace(text))
            {
                return 0;
            }

            var textTrimmedAtEnd = text.TrimEnd();
            var sentenceSeparators = new[] { '.', '!', '?' };
            var sentences = textTrimmedAtEnd.Split(sentenceSeparators, StringSplitOptions.RemoveEmptyEntries);

            if (sentences.Length == 0)
            {
                return 0;
            }

            var sentencesLengthSum = sentences.Sum(x => x.Length);
            var averageSentenceLength = sentencesLengthSum / sentences.Length;

            return averageSentenceLength;
        }

        private string[] SplitByWords(string text)
        {
            if (string.IsNullOrEmpty(text))
            {
                return null;
            }

            var trimmedText = text.Trim();
            var wordSeparators = new[] { ' ', ',', '.', ':', '!', '&', '?', ';', '/', '*' };
            var words = trimmedText.Split(wordSeparators, StringSplitOptions.RemoveEmptyEntries);

            return words;
        }
    }
}