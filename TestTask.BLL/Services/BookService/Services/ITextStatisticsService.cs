﻿namespace TestTask.BLL.Services.BookService.Services
{
    public interface ITextStatisticsService
    {
        int TextLength(string text);
        int WordsCount(string text);
        int UniqueWordsCount(string text);
        int AverageWordLength(string text);
        int AverageSentenceLength(string text);
    }
}