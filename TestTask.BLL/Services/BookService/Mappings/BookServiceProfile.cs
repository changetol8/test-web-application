﻿using AutoMapper;
using TestTask.BLL.Services.AuthorService.Dtos;
using TestTask.BLL.Services.BookService.Dtos;
using TestTask.BLL.Services.GenreService.Dtos;
using TestTask.DAL.Entities;
using TestTask.DAL.Repositories.Queries;

namespace TestTask.BLL.Services.BookService.Mappings
{
    public class BookServiceProfile : Profile
    {
        public BookServiceProfile()
        {
            CreateMap<BookDto, Book>().ReverseMap();
            CreateMap<GenreDto, Genre>().ReverseMap();
            CreateMap<AuthorDto, Author>().ReverseMap();
            CreateMap<SearchBookQuery, SearchBookDto>().ReverseMap();
        }
    }
}