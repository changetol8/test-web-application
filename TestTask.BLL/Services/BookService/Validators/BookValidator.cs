﻿using FluentValidation;
using TestTask.BLL.Services.BookService.Dtos;
using TestTask.BLL.Properties;

namespace TestTask.BLL.Services.BookService.Validation
{
    public class BookValidator : AbstractValidator<BookDto>
    {
        public BookValidator()
        {
            RuleFor(book => book.Name)
                .NotEmpty()
                .Length(int.Parse(BookValidationValues.MinBookNameLength),
                        int.Parse(BookValidationValues.MaxBookTextLength));

            RuleFor(book => book.CreationDate)
                .NotNull();

            RuleFor(book => book.Text)
                .NotEmpty()
                .Length(int.Parse(BookValidationValues.MinBookTextLength),
                        int.Parse(BookValidationValues.MaxBookTextLength));

            RuleFor(book => book.Description)
                .NotEmpty()
                .Length(int.Parse(BookValidationValues.MinBookDescriptionLength),
                        int.Parse(BookValidationValues.MaxBookDescriptionLength));

            RuleFor(book => book.Authors)
                .NotNull();

            RuleFor(book => book.Genres)
                .NotNull();
        }
    }
}