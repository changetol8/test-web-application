﻿using Ninject.Modules;
using Ninject.Web.Common;
using TestTask.DAL.EF;
using TestTask.DAL.Interfaces;
using TestTask.DAL.Repositories;

namespace TestTask.BLL.Infrastructure 
{
    public class ServiceModule : NinjectModule
    {
        private readonly string _connectionString;

        public ServiceModule(string connectionString)
        {
            _connectionString = connectionString;
        }

        public override void Load()
        {
            Bind<LibraryContext>().ToSelf()
                .InRequestScope()
                .WithConstructorArgument(_connectionString);

            Bind<IBookRepository>().To<BookRepository>();
            Bind<IAuthorRepository>().To<AuthorRepository>();
            Bind<IGenreRepository>().To<GenreRepository>();
        }
    }
}