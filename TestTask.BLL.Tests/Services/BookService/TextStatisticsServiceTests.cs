﻿using NUnit.Framework;
using TestTask.BLL.Services.BookService.Services;

namespace TestTask.BLL.Tests.Services.BookService
{
    [TestFixture]
    public class TextStatisticsServiceTests
    {
        private readonly TextStatisticsService _statistics = new TextStatisticsService();

        #region TextLengthTests

        [Test]
        [TestCase("")]
        [TestCase(null)]
        public void TextLength_EmptyOrNull_MustReturnZero(string text)
        {
            // Act
            var actual = _statistics.TextLength(text);

            // Assert
            Assert.That(actual, Is.Zero);
        }

        [Test]
        [TestCase("The.", 4)]
        [TestCase("The one", 7)]
        [TestCase("  ", 0)]
        public void TextLength_CorrectText_MustWorkCorrectly(string text, int expected)
        {
            // Act
            var actual = _statistics.TextLength(text);

            // Assert
            Assert.That(actual, Is.EqualTo(expected));
        }

        [Test]
        [TestCase("The.  ", 4)]
        [TestCase("  One.  ", 4)]
        [TestCase("   Three", 5)]
        public void TextLength_TextWithSpaces_MustRemoveSpaces(string text, int expected)
        {
            // Act
            var actual = _statistics.TextLength(text);

            // Assert
            Assert.That(actual, Is.EqualTo(expected));
        }

        #endregion

        #region WordCountTests

        [Test]
        [TestCase("")]
        [TestCase(null)]
        public void WordCount_EmptyOrNull_MustReturnZero(string text)
        {
            // Act
            var actual = _statistics.WordsCount(text);

            // Assert
            Assert.That(actual, Is.Zero);
        }

        [Test]
        [TestCase("One two.", 2)]
        [TestCase("....", 0)]
        [TestCase("   ", 0)]
        public void WordCount_Words_MustWorkCorrectly(string text, int expected)
        {
            // Act
            var actual = _statistics.WordsCount(text);

            // Assert
            Assert.That(actual, Is.EqualTo(expected));
        }

        [Test]
        [TestCase(" One  two three  .  ", 3)]
        [TestCase("   one", 1)]
        [TestCase("Word;...  ", 1)]
        public void WordCount_WordsWithSpaces_MustWorkCorrectly(string text, int expected)
        {
            // Act
            var actual = _statistics.WordsCount(text);

            // Assert
            Assert.That(actual, Is.EqualTo(expected));
        }

        #endregion

        #region UniqueWordsTests

        [Test]
        [TestCase("")]
        [TestCase(null)]
        public void UniqueWords_EmptyOrNull_MustReturnZero(string text)
        {
            // Act
            var actual = _statistics.UniqueWordsCount(text);

            // Assert
            Assert.That(actual, Is.Zero);
        }

        [Test]
        [TestCase("One two two.", 2)]
        [TestCase("One one", 1)]
        [TestCase("    ", 0)]
        public void UniqueWords_TextWithWords_MustWorkCorrectly(string text, int expected)
        {
            // Act
            var actual = _statistics.UniqueWordsCount(text);

            // Assert
            Assert.That(actual, Is.EqualTo(expected));
        }

        [Test]
        [TestCase(" One  two   five five.  ", 3)]
        [TestCase("  One;  two; two... ", 2)]
        [TestCase(" ..One...,,,,two&two", 2)]
        public void UniqueWords_WordsWithSpacesAndSeparators_MustWorkCorrectly(string text, int expected)
        {
            // Act
            var actual = _statistics.UniqueWordsCount(text);

            // Assert
            Assert.That(actual, Is.EqualTo(expected));
        }

        #endregion

        #region AverageWordLengthTests

        [Test]
        [TestCase("")]
        [TestCase(null)]
        public void AverageWordLength_IfTextEmptyOrNull_MustReturnZero(string text)
        {
            // Act
            var actual = _statistics.AverageWordLength(text);

            // Assert
            Assert.That(actual, Is.Zero);
        }

        [Test]
        [TestCase("One two.  ", 3)]
        [TestCase("One,  one, two.", 3)]
        public void AverageWordLength_IfWordsWithSpacesAndSeparators_MustWorkCorrectly(string text, int expected)
        {
            // Act
            var actual = _statistics.AverageWordLength(text);

            // Assert
            Assert.That(actual, Is.EqualTo(expected));
        }

        [Test]
        [TestCase("    ")]
        [TestCase(".......")]
        [TestCase(",,,,,,")]
        [TestCase(",,:,,,..")]
        public void AverageWordLength_IfTextOnlyWithSpacesOrSeparators_MustReturnZero(string text)
        {
            // Act
            var actual = _statistics.AverageWordLength(text);

            // Assert
            Assert.That(actual, Is.Zero);
        }

        #endregion

        #region AverageSentenceLengthTests

        [Test]
        [TestCase("")]
        [TestCase(null)]
        public void AverageSentenceLength_IfTextEmptyOrNull_MustReturnZero(string text)
        {
            // Act
            var actual = _statistics.AverageSentenceLength(text);

            // Assert
            Assert.That(actual, Is.Zero);

        }

        [Test]
        [TestCase("Second", 6)]
        [TestCase("One.", 3)]
        [TestCase("Two,.", 4)]
        public void AverageSentenceLength_CorrectText_MustWorkCorrectly(string text, int expected)
        {
            // Act 
            var actual = _statistics.AverageSentenceLength(text);

            // Assert
            Assert.That(actual, Is.EqualTo(expected));
        }

        [Test]
        [TestCase("One.Three", 4)]
        [TestCase("Two.two.", 3)]
        [TestCase("Hello.I.", 3)]
        public void AverageSentenceLength_CorrectTextWithSeveralSentences_MustWorkCorrectly(string text, int expected)
        {
            // Act
            var actual = _statistics.AverageSentenceLength(text);

            // Assert
            Assert.That(actual, Is.EqualTo(expected));
        }

        [Test]
        [TestCase(" Word. ", 5)]
        [TestCase("Word;.", 5)]
        public void AverageSentenceLength_IfTextWithSpacesOrSeparators_MustWorkCorrectly(string text, int expected)
        {
            // Act
            var actual = _statistics.AverageSentenceLength(text);

            // Assert
            Assert.That(actual, Is.EqualTo(expected));
        }

        [Test]
        [TestCase("...")]
        [TestCase("?????")]
        [TestCase("!!!!!!")]
        public void AverageSentenceLength_IfTextOnlyWithSeparators_MustReturnZero(string text)
        {
            // Act
            var actual = _statistics.AverageSentenceLength(text);

            // Assert
            Assert.That(actual, Is.Zero);
        }

        #endregion
    }
}