﻿using AutoMapper;
using Ninject.Modules;
using TestTask.BLL.Services.AuthorService;
using TestTask.BLL.Services.AuthorService.Mappings;
using TestTask.BLL.Services.BookService;
using TestTask.BLL.Services.BookService.Mappings;
using TestTask.BLL.Services.BookService.Services;
using TestTask.BLL.Services.GenreService;
using TestTask.BLL.Services.GenreService.Mappings;
using TestTask.BLL.Services.PagingService;
using TestTask.UI.Infrastructure.Mappings;

namespace TestTask.UI.Utils
{
    public class LibraryModule : NinjectModule
    {
        public override void Load()
        {
            var mapperConfiguration = CreateConfiguration();
            Bind<MapperConfiguration>().ToConstant(mapperConfiguration).InSingletonScope();

            Bind<IMapper>().ToMethod(ctx => new Mapper(mapperConfiguration));

            Bind<IAuthorService>().To<AuthorService>();
            Bind<IGenreService>().To<GenreService>();
            Bind<IBookService>().To<BookService>();
            Bind<IPagingService>().To<PagingService>();
            Bind<ITextStatisticsService>().To<TextStatisticsService>();
        }

        private static MapperConfiguration CreateConfiguration()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<AuthorServiceProfile>();
                cfg.AddProfile<BookServiceProfile>();
                cfg.AddProfile<GenreServiceProfile>();

                cfg.AddProfile<AuthorViewProfile>();
                cfg.AddProfile<BookViewProfile>();
                cfg.AddProfile<GenreViewProfile>();
            });

            return config;
        }
    }
}