﻿using AutoMapper;
using System.Web.Mvc;
using TestTask.BLL.Services.AuthorService;
using TestTask.UI.Models;

namespace TestTask.UI.Controllers
{
    public class AuthorController : Controller
    {
        private readonly IAuthorService _authorService;
        private readonly IMapper _mapper;

        public AuthorController(IAuthorService authorService,
                                IMapper mapper)
        {
            _authorService = authorService;
            _mapper = mapper;
        }

        public ActionResult Index(int? itemsCount, int? page)
        {
            var authorsWithPageDto = _authorService.GetAuthorsWithPageDto(page, itemsCount);
            var authorsWithPageViewModel = _mapper.Map<AuthorsWithPageViewModel>(authorsWithPageDto);
            
            return View(authorsWithPageViewModel);
        }
    }
}