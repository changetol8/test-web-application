﻿using System.Collections.Generic;
using System.Web.Http;
using TestTask.BLL.Services.BookService;
using TestTask.BLL.Services.BookService.Dtos;

namespace TestTask.UI.Controllers.Api
{
    public class BooksController : ApiController
    {
        private readonly IBookService _bookService;

        public BooksController(IBookService bookService)
        {
            _bookService = bookService;
        }

        [HttpGet]
        [Route("api/books")]
        public List<BookDto> GetBooks([FromUri] int skip, [FromUri] int take)
        {
            var books = _bookService.GetBooks(skip, take, null);

            return books;
        }

        [HttpGet]
        [Route("api/books/{id}")]
        public BookDto GetBook([FromUri] int id)
        {
            var book = _bookService.GetBook(id);

            return book;
        }

        [HttpPost]
        [Route("api/books")]
        public void CreateBook([FromBody] BookDto book)
        {
            _bookService.CreateBook(book);
        }

        [HttpPut]
        [Route("api/books/{id}")]
        public void UpdateBook([FromBody] BookDto book)
        {
            _bookService.UpdateBook(book);
        }
    }
}