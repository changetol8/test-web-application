﻿using System.Collections.Generic;
using System.Web.Http;
using TestTask.BLL.Services.AuthorService;
using TestTask.BLL.Services.AuthorService.Dtos;

namespace TestTask.UI.Controllers.Api
{
    public class AuthorsController : ApiController
    {
        private readonly IAuthorService _authorService;

        public AuthorsController(IAuthorService authorService)
        {
            _authorService = authorService;
        }

        [HttpGet]
        [Route("api/authors")]
        public List<AuthorWithoutBooksDto> GetAuthors([FromUri] int skip, [FromUri] int take, [FromUri] string name)
        {
            var authors = _authorService.GetAuthorsWithoutBooks(skip, take, name);

            return authors;
        }

        [HttpGet]
        [Route("api/authors/{id}")]
        public AuthorDto GetAuthor([FromUri] int id)
        {
            var author = _authorService.GetAuthorById(id);

            return author;
        }
    }
}