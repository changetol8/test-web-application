﻿using System.Collections.Generic;
using System.Web.Http;
using TestTask.BLL.Services.GenreService;
using TestTask.BLL.Services.GenreService.Dtos;

namespace TestTask.UI.Controllers.Api
{
    public class GenresController : ApiController
    {
        private readonly IGenreService _genreService;

        public GenresController(IGenreService genreService)
        {
            _genreService = genreService;
        }

        [HttpGet]
        [Route("api/genres")]
        public List<GenreWithoutBooksDto> GetGenres([FromUri] int skip, [FromUri] int take, [FromUri] string name)
        {
            var genres = _genreService.GetGenresWithoutBooks(skip, take, name);

            return genres;
        }

        [HttpGet]
        [Route("api/genres/{id}")]
        public GenreDto GetGenre([FromUri] int id)
        {
            var genre = _genreService.GetGenreById(id);

            return genre;
        }
    }
}