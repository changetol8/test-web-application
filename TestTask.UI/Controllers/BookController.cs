﻿using AutoMapper;
using System.Web.Mvc;
using TestTask.BLL.Services.BookService;
using TestTask.BLL.Services.BookService.Dtos;
using TestTask.UI.Models;

namespace TestTask.UI.Controllers
{
    public class BookController : Controller
    {
        private readonly IBookService _bookService;
        private readonly IMapper _mapper;

        public BookController(IBookService bookService,
                              IMapper mapper)
        {
            _bookService = bookService;
            _mapper = mapper;
        }

        [HttpGet]
        public ActionResult Index(int? page, int? itemsCount)
        {
            var booksWithPageDto = _bookService.GetBooksWithPageDto(page, itemsCount);
            var booksWithPageViewModel = _mapper.Map<BooksWithPageViewModel>(booksWithPageDto);

            return View(booksWithPageViewModel);
        }

        [HttpPost]
        public ActionResult Index(int? page, int? itemsCount, SearchBookViewModel searchBook)
        {
            var searchBookDto = _mapper.Map<SearchBookDto>(searchBook);
            var booksWithPageDto = _bookService.GetBooksWithPageDto(page, itemsCount, searchBookDto);
            var booksWithPageViewModel = _mapper.Map<BooksWithPageViewModel>(booksWithPageDto);

            return View(booksWithPageViewModel);
        }

        [HttpGet]
        public ActionResult AddBook()
        {
            var bookDto = new BookDto();
            var bookViewModel = _mapper.Map<BookViewModel>(bookDto);

            return View(bookViewModel);
        }

        [HttpPost]
        public ActionResult AddBook(BookViewModel book)
        {
            var bookDto = _mapper.Map<BookDto>(book);

            if (!ModelState.IsValid)
            {
                return View(book);
            }

            _bookService.CreateBook(bookDto);

            return RedirectToAction(nameof(Index));
        }

        public ActionResult BookDetails(int id)
        {
            var bookDto = _bookService.GetBook(id);
            var bookViewModel = _mapper.Map<BookViewModel>(bookDto);

            return PartialView($"_{nameof(BookDetails)}", bookViewModel);
        }

        public ActionResult EditBook(int id)
        {
            var bookDto = _bookService.GetBook(id);
            var bookViewModel = _mapper.Map<BookViewModel>(bookDto);

            return View(bookViewModel);
        }

        [HttpPost]
        public ActionResult EditBook(BookViewModel book)
        {
            var bookDto = _mapper.Map<BookDto>(book);

            if (!ModelState.IsValid)
            {
                var bookView = _mapper.Map<BookViewModel>(bookDto);

                return View(bookView);
            }

            _bookService.UpdateBook(bookDto);

            return RedirectToAction(nameof(Index));
        }
    }
}