﻿using AutoMapper;
using System.Web.Mvc;
using TestTask.BLL.Services.GenreService;
using TestTask.UI.Models;

namespace TestTask.UI.Controllers
{
    public class GenreController : Controller
    {
        private readonly IGenreService _genreService;
        private readonly IMapper _mapper;

        public GenreController(IGenreService genreService,
                               IMapper mapper)
        {
            _genreService = genreService;
            _mapper = mapper;
        }

        public ActionResult Index(int? page, int? itemsCount)
        {
            var genresWithPageDto = _genreService.GetGenresWithPageDto(page, itemsCount);
            var genresWithPageViewModel = _mapper.Map<GenresWithPageViewModel>(genresWithPageDto);

            return View(genresWithPageViewModel);
        }
    }
}