﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace TestTask.UI.Models
{
    public class GenreViewModel
    {
        public int GenreId { get; set; }

        [Required]
        [Display(Name = "Название")]
        public string Name { get; set; }

        public List<BookViewModel> Books { get; set; }
    }
}