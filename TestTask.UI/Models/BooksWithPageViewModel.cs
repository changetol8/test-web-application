﻿using System.Collections.Generic;

namespace TestTask.UI.Models
{
    public class BooksWithPageViewModel
    {
        public List<BookViewModel> Books { get; set; }
        public SearchBookViewModel SearchBook { get; set; }
        public PageInfoViewModel PageInfo{ get; set; }
    }
}