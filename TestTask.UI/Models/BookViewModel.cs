﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace TestTask.UI.Models
{
    public class BookViewModel
    {
        public int BookId { get; set; }

        [Display(Name="Название")]
        public string Name { get; set; }

        [Display(Name = "Описание")]
        public string Description { get; set; }

        [Display(Name="Дата написания")]
        public DateTime? CreationDate { get; set; }

        [Display(Name="ISBN")]
        public string Isbn { get; set; }

        [Display(Name="Содержание")]
        public string Text { get; set; }

        public BookStatisticsModel BookStatistics { get; set; }

        public List<AuthorViewModel> Authors { get; set; }

        public List<GenreViewModel> Genres { get; set; }

        public List<int> AuthorsIds { get; set; }

        public List<int> GenresIds { get; set; }
    }
}