﻿using System;

namespace TestTask.UI.Models
{
    public class SearchBookViewModel
    {
        public string BookName { get; set; }
        public string AuthorName { get; set; }
        public string GenreName { get; set; }
        public DateTime? CreationDate { get; set; }
    }
}