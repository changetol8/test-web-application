﻿namespace TestTask.UI.Models
{
    public class BookStatisticsModel
    {
        public int BookTextLength { get; set; }
        public int BookWordsCount { get; set; } 
        public int BookUniqueWordsCount { get; set; }
        public int BookAverageWordLength { get; set; }
        public int BookAverageSentenceLength { get; set; }
    }
}