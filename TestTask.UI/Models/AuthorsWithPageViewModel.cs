﻿using System.Collections.Generic;

namespace TestTask.UI.Models
{
    public class AuthorsWithPageViewModel
    {
        public List<AuthorViewModel> Authors { get; set; }
        public PageInfoViewModel PageInfo { get; set; }
    }
}