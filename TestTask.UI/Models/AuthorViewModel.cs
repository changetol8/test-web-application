﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace TestTask.UI.Models
{
    public class AuthorViewModel
    {
        public int AuthorId { get; set; }

        [Required]
        [Display(Name = "Фамилия")]
        public string Surname { get; set; }

        [Required]
        [Display(Name = "Имя")]
        public string Name { get; set; }

        [Required]
        [Display(Name = "Отчество")]
        public string Patronymic { get; set; }

        [Required]
        [Display(Name = "Дата рождения")]
        public DateTime? BirthDate { get; set; }

        public List<BookViewModel> Books { get; set; }
    }
}