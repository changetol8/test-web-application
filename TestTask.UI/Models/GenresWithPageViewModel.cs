﻿using System.Collections.Generic;

namespace TestTask.UI.Models
{
    public class GenresWithPageViewModel
    {
        public List<GenreViewModel> Genres { get; set; }
        public PageInfoViewModel PageInfo { get; set; }
    }
}