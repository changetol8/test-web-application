﻿namespace TestTask.UI.Models
{
    public class PageInfoViewModel
    {
        public int PageNumber{ get; set; }
        public int ItemsCountPerPage{ get; set; }
        public int TotalItemsCount { get; set; }
        public int PagesCount { get; set; }
    }
}