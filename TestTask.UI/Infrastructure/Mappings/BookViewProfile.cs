﻿using AutoMapper;
using TestTask.BLL.Services.AuthorService.Dtos;
using TestTask.BLL.Services.BookService.Dtos;
using TestTask.BLL.Services.GenreService.Dtos;
using TestTask.BLL.Services.PagingService.Dtos;
using TestTask.UI.Models;

namespace TestTask.UI.Infrastructure.Mappings
{
    public class BookViewProfile : Profile
    {
        public BookViewProfile()
        {
            CreateMap<AuthorDto, AuthorViewModel>();
            CreateMap<GenreDto, GenreViewModel>();
            CreateMap<BookDto, BookViewModel>();
            CreateMap<int, AuthorWithoutBooksDto>()
                .ForMember(dest => dest.AuthorId,
                opt => opt.MapFrom(src => src));
            CreateMap<int, GenreWithoutBooksDto>()
                .ForMember(dest => dest.GenreId,
                opt => opt.MapFrom(src => src));
            CreateMap<BookStatisticsDto, BookStatisticsModel>();
            CreateMap<BookStatisticsModel, BookStatisticsDto>();
            CreateMap<BookViewModel, BookDto>()
                .ForMember(dest => dest.Authors,
                    opt => opt.MapFrom(src => src.AuthorsIds))
                .ForMember(dest => dest.Genres,
                    opt => opt.MapFrom(src => src.GenresIds));
            CreateMap<SearchBookDto, SearchBookViewModel>();
            CreateMap<SearchBookViewModel, SearchBookDto>();
            CreateMap<PageInfoDto, PageInfoViewModel>();
            CreateMap<PageInfoViewModel, PageInfoDto>();
            CreateMap<BooksWithPageDto, BooksWithPageViewModel>();
        }
    }
}