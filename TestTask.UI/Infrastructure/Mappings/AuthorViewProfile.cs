﻿using AutoMapper;
using TestTask.BLL.Services.AuthorService.Dtos;
using TestTask.BLL.Services.BookService.Dtos;
using TestTask.BLL.Services.GenreService.Dtos;
using TestTask.UI.Models;

namespace TestTask.UI.Infrastructure.Mappings
{
    public class AuthorViewProfile : Profile
    {
        public AuthorViewProfile()
        {
            CreateMap<BookDto, BookViewModel>();
            CreateMap<GenreDto, GenreViewModel>();
            CreateMap<AuthorDto, AuthorViewModel>();
            CreateMap<AuthorsWithPageDto, AuthorsWithPageViewModel>();
            CreateMap<AuthorDto, AuthorWithoutBooksDto>();
            CreateMap<AuthorWithoutBooksDto, AuthorViewModel>();
        }
    }
}