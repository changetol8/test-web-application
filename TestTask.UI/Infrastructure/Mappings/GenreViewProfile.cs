﻿using AutoMapper;
using TestTask.BLL.Services.AuthorService.Dtos;
using TestTask.BLL.Services.BookService.Dtos;
using TestTask.BLL.Services.GenreService.Dtos;
using TestTask.UI.Models;

namespace TestTask.UI.Infrastructure.Mappings
{
    public class GenreViewProfile : Profile
    {
        public GenreViewProfile()
        {
            CreateMap<AuthorDto, AuthorViewModel>();
            CreateMap<BookDto, BookViewModel>();
            CreateMap<GenreDto, GenreViewModel>();
            CreateMap<GenresWithPageDto, GenresWithPageViewModel>();
            CreateMap<GenreWithoutBooksDto, GenreViewModel>();
        }
    }
}