﻿$(document).ready(function () {
    $("#select2-genres").select2({
        placeholder: "Выберите из жанров",
        allowClear: true,
        multiple: true,
        minimumInputLength: 1,
        ajax: {
            delay: 250,
            url: "/api/genres",
            dataType: "json",
            data: function (params) {
                var query = {
                    skip: skip = 0,
                    take: take = 10,
                    name: params.term
                }

                return query;
            },
            processResults: function (data, params) {
                return {
                    results: $.map(data, function (item) {
                        return {
                            id: item.GenreId,
                            text: item.Name
                        }
                    })
                }
            }
        }
    });
});