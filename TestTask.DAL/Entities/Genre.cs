﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TestTask.DAL.Entities
{
    public class Genre
    {
        [Key]
        public int GenreId { get; set; }

        [StringLength(50)]
        [Index]
        public string Name { get; set; }

        public virtual List<Book> Books { get; set; }
    }
}