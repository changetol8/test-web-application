﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TestTask.DAL.Entities
{
    public class Author
    {
        [Key]
        public int AuthorId { get; set; }

        [StringLength(50)]
        [Index]
        public string Surname { get; set; }

        [StringLength(50)]
        [Index]
        public string Name { get; set; }

        public string Patronymic { get; set; }

        public DateTime? BirthDate { get; set; }

        public virtual List<Book> Books { get; set; }
    }
}