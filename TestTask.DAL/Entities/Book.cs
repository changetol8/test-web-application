﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TestTask.DAL.Entities
{
    public class Book
    {
        [Key]
        public int BookId { get; set; }

        [StringLength(50)]
        [Index]
        public string Name { get; set; }

        public string Description { get; set; }

        public DateTime? CreationDate { get; set; }

        public string Isbn { get; set; }

        public string Text { get; set; }

        public List<Author> Authors { get; set; }

        public List<Genre> Genres { get; set; }
    }
}