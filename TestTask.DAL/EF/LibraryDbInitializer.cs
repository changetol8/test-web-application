﻿using System.Data.Entity;
using TestTask.DAL.DataTableCreators;
using TestTask.DAL.SqlCopy;

namespace TestTask.DAL.EF
{
    public class LibraryDbInitializer : DropCreateDatabaseAlways<LibraryContext>
    {
        protected override void Seed(LibraryContext db)
        {
            db.Database.CreateIfNotExists();

            var connectionString = db.Database.Connection.ConnectionString;
            var sqlCopy = new SqlBulkCopyFacade(connectionString);
            var dataTableCreator = new DataTableCreator();

            const int authorsCount = 10_000;
            var authorsTable = dataTableCreator.CreateAuthorsTable(authorsCount);
            sqlCopy.FillDatabase(authorsTable);

            const int booksCount = 100_000;
            var booksTable = dataTableCreator.CreateBooksTable(booksCount);
            sqlCopy.FillDatabase(booksTable);

            const int genresCount = 100;
            var genresTable = dataTableCreator.CreateGenresTable(genresCount);
            sqlCopy.FillDatabase(genresTable);

            var bookAuthorsTable = dataTableCreator.CreateBookAuthorsTable(booksCount, authorsCount);
            sqlCopy.FillDatabase(bookAuthorsTable);

            var bookGenresTable = dataTableCreator.CreateBookGenresTable(booksCount, genresCount);
            sqlCopy.FillDatabase(bookGenresTable);
        }
    }
}