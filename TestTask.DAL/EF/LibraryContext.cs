﻿using System.Data.Entity;
using TestTask.DAL.Entities;

namespace TestTask.DAL.EF
{
    public class LibraryContext : DbContext
    {
        public LibraryContext(string connectionString)
            : base(connectionString)
        {
            Configuration.LazyLoadingEnabled = false;
        }

        public DbSet<Book> Books { get; set; }
        public DbSet<Author> Authors { get; set; }
        public DbSet<Genre> Genres { get; set; }
    }
}