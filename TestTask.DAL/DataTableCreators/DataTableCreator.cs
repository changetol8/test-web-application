﻿using System;
using System.Data;

namespace TestTask.DAL.DataTableCreators
{
    public class DataTableCreator
    {
        public DataTable CreateBooksTable(int recordsCount)
        {
            var dataTable = new DataTable("Books");

            const string bookId = "BookId";
            const string name = "Name";
            const string description = "Description";
            const string creationDate = "CreationDate";
            const string isbn = "ISBN";
            const string text = "Text";

            var columns = new[] { name, description, creationDate, isbn, text };

            foreach (var column in columns)
            {
                dataTable.Columns.Add(column);
            }

            dataTable.Columns.Add(bookId, typeof(int));

            var random = new Random();

            for (var i = 1; i <= recordsCount; i++)
            {
                var row = dataTable.NewRow();

                row[bookId] = i;
                row[name] = "BookName" + i;
                row[description] = "Description" + i;
                row[creationDate] = DateTime.Now;
                row[isbn] = "978" + random.Next(100, 999) + random.Next(123, 1344);
                row[text] = "Content. This sentences describes and tells about some characters.";

                dataTable.Rows.Add(row);
            }

            return dataTable;
        }

        public DataTable CreateAuthorsTable(int recordsCount)
        {
            var dataTable = new DataTable("Authors");

            const string authorId = "AuthorId";
            const string surname = "Surname";
            const string name = "Name";
            const string patronymic = "Patronymic";
            const string birthDate = "BirthDate";

            var columns = new[] { surname, name, patronymic, birthDate };

            foreach (var column in columns)
            {
                dataTable.Columns.Add(column);
            }

            dataTable.Columns.Add(authorId, typeof(int));

            for (var i = 1; i <= recordsCount; i++)
            {
                var row = dataTable.NewRow();

                row[authorId] = i;
                row[surname] = "Surname" + i;
                row[name] = "Name" + i;
                row[patronymic] = "Patronymic" + i;
                row[birthDate] = DateTime.Now;

                dataTable.Rows.Add(row);
            }

            return dataTable;
        }

        public DataTable CreateGenresTable(int recordsCount)
        {
            var dataTable = new DataTable("Genres");

            const string name = "Name";

            var columns = new[] { name };

            foreach (var column in columns)
            {
                dataTable.Columns.Add(column);
            }

            for (var i = 1; i <= recordsCount; i++)
            {
                var row = dataTable.NewRow();
                row[name] = "GenreName" + i;

                dataTable.Rows.Add(row);
            }

            return dataTable;
        }

        public DataTable CreateBookAuthorsTable(int booksCount, int authorsCount)
        {
            var dataTable = new DataTable("BookAuthors");

            const string bookId = "Book_BookId";
            const string authorId = "Author_AuthorId";

            var columns = new[] { bookId, authorId };

            foreach (var column in columns)
            {
                dataTable.Columns.Add(column, typeof(int));
            }

            for (var i = 1; i <= booksCount; i++)
            {
                var row = dataTable.NewRow();

                row[bookId] = i;

                if (i <= authorsCount)
                {
                    row[authorId] = i;
                }
                else
                {
                    row[authorId] = 0;
                }

                dataTable.Rows.Add(row);
            }

            return dataTable;
        }

        public DataTable CreateBookGenresTable(int booksCount, int genresCount)
        {
            var dataTable = new DataTable("GenreBooks");

            const string bookId = "Book_BookId";
            const string genreId = "Genre_GenreId";

            var columns = new[] { bookId, genreId };

            foreach (var column in columns)
            {
                dataTable.Columns.Add(column, typeof(int));
            }

            for (var i = 1; i <= booksCount; i++)
            {
                var row = dataTable.NewRow();

                row[bookId] = i;

                if (i <= genresCount)
                {
                    row[genreId] = i;
                }
                else
                {
                    row[genreId] = 0;
                }

                dataTable.Rows.Add(row);
            }

            return dataTable;
        }
    }
}