﻿using System.Collections.Generic;
using TestTask.DAL.Entities;
using TestTask.DAL.Repositories.Queries;

namespace TestTask.DAL.Interfaces
{
    public interface IGenreRepository
    {
        int GetGenresTotalCount();
        Genre GetGenreById(int genreId);
        List<Genre> GetGenres(int skip, int take, GetGenresQuery query = null);
    }
}