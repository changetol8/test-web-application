﻿using System.Collections.Generic;
using TestTask.DAL.Entities;
using TestTask.DAL.Repositories.Queries;

namespace TestTask.DAL.Interfaces
{
    public interface IAuthorRepository 
    {
        int GetAuthorsTotalCount();
        Author GetAuthorById(int authorId);
        List<Author> GetAuthors(int skip, int take, GetAuthorsQuery query = null);
    }
}