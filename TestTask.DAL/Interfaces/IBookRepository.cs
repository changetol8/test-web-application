﻿using System.Collections.Generic;
using TestTask.DAL.Entities;
using TestTask.DAL.Repositories.Queries;

namespace TestTask.DAL.Interfaces
{
    public interface IBookRepository
    {
        void CreateBook(Book book);
        void UpdateBook(Book bookForUpdate);
        int GetBooksTotalCount();
        Book GetBookById(int bookId);
        List<Book> GetBooks(int skip, int take, SearchBookQuery query = null);
    }
}