﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using TestTask.DAL.EF;
using TestTask.DAL.Entities;
using TestTask.DAL.Interfaces;
using TestTask.DAL.Repositories.Queries;

namespace TestTask.DAL.Repositories
{
    public class AuthorRepository : IAuthorRepository
    {
        private readonly LibraryContext _libraryContext;

        public AuthorRepository(LibraryContext libraryContext)
        {
            _libraryContext = libraryContext;
        }

        public int GetAuthorsTotalCount()
        {
            var authorsCount = _libraryContext.Authors.Count();

            return authorsCount;
        }

        public Author GetAuthorById(int authorId)
        {
            var author = _libraryContext.Authors.FirstOrDefault(x => x.AuthorId == authorId);

            return author;
        }

        public List<Author> GetAuthors(int skip, int take, GetAuthorsQuery query = null)
        {
            var authors = _libraryContext.Authors.Include(x => x.Books);

            if (!string.IsNullOrWhiteSpace(query?.AuthorName))
            {
                authors = authors.Where(x => x.Name.Contains(query.AuthorName));
            }

            var result = authors.OrderBy(x => x.AuthorId).Skip(skip).Take(take).ToList();
            return result;
        }
    }
}