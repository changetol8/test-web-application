﻿using System;

namespace TestTask.DAL.Repositories.Queries
{
    public class SearchBookQuery
    {
        public string BookName { get; set; }
        public string AuthorName { get; set; }
        public string GenreName { get; set; }
        public DateTime? CreationDate { get; set; }
    }
}