﻿namespace TestTask.DAL.Repositories.Queries
{
    public class GetAuthorsQuery
    {
        public string AuthorName { get; set; }
    }
}