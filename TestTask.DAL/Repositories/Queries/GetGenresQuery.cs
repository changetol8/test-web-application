﻿namespace TestTask.DAL.Repositories.Queries
{
    public class GetGenresQuery
    {
        public string GenreName { get; set; }
    }
}