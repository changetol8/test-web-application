﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using TestTask.DAL.EF;
using TestTask.DAL.Entities;
using TestTask.DAL.Interfaces;
using TestTask.DAL.Repositories.Queries;

namespace TestTask.DAL.Repositories
{
    public class BookRepository : IBookRepository
    {
        private readonly LibraryContext _libraryContext;

        public BookRepository(LibraryContext libraryContext)
        {
            _libraryContext = libraryContext;
        }

        public void CreateBook(Book book)
        {
            foreach (var author in book.Authors)
            {
                _libraryContext.Authors.Attach(author);
            }

            foreach (var genre in book.Genres)
            {
                _libraryContext.Genres.Attach(genre);
            }

            _libraryContext.Books.Add(book);
            _libraryContext.SaveChanges();
        }

        public Book GetBookById(int bookId)
        {
            var book = _libraryContext.Books
                .Include(x => x.Authors)
                .Include(x => x.Genres)
                .FirstOrDefault(x => x.BookId == bookId);

            return book;
        }

        public int GetBooksTotalCount()
        {
            var booksTotalCount = _libraryContext.Books.Count();

            return booksTotalCount;
        }

        public List<Book> GetBooks(int skip, int take, SearchBookQuery query = null)
        {
            var books = _libraryContext.Books
                .Include(x => x.Authors)
                .Include(x => x.Genres)
                .OrderBy(x => x.BookId)
                .AsQueryable();

            if (query == null)
            {
                return books.Skip(skip).Take(take).ToList();
            }

            if (!string.IsNullOrWhiteSpace(query.BookName))
            {
                books = books.Where(x => x.Name.Contains(query.BookName));
            }

            if (!string.IsNullOrWhiteSpace(query.AuthorName))
            {
                books = GetBooksByAuthorNameQuery(query.AuthorName, books);
            }

            if (!string.IsNullOrWhiteSpace(query.GenreName))
            {
                books = books.Where(x => x.Genres.Any(y => y.Name.Contains(query.GenreName)));
            }

            if (query.CreationDate != null)
            {
                books = books.Where(x => x.CreationDate.Value.Year == query.CreationDate.Value.Year);
            }

            var result = books.Skip(skip).Take(take).ToList();
            return result;
        }

        private IQueryable<Book> GetBooksByAuthorNameQuery(string authorName, IQueryable<Book> books)
        {
            var parsedAuthorName = authorName.Split(' ');

            if (parsedAuthorName.Length == 1)
            {
                var firstValue = parsedAuthorName[0];

                books = books.Where(x => x.Authors.Any(y =>
                    y.Surname.Contains(firstValue) || y.Name.Contains(firstValue)));
            }
            else if (parsedAuthorName.Length == 2)
            {
                var firstValue = parsedAuthorName[0];
                var secondValue = parsedAuthorName[1];

                books = books.Where(x => x.Authors.Any(y =>
                    y.Surname.Contains(firstValue) && y.Name.Contains(secondValue) ||
                    y.Surname.Contains(secondValue) && y.Name.Contains(firstValue)));
            }
            else if (parsedAuthorName.Length == 3)
            {
                var firstValue = parsedAuthorName[0];
                var secondValue = parsedAuthorName[1];
                var thirdValue = parsedAuthorName[2];

                books = books.Where(x => x.Authors.Any(y =>
                    y.Surname.Contains(firstValue) && y.Name.Contains(secondValue) && y.Patronymic.Contains(thirdValue) ||
                    y.Surname.Contains(secondValue) && y.Name.Contains(firstValue) && y.Patronymic.Contains(thirdValue)));
            }

            return books;
        }

        public void UpdateBook(Book bookForUpdate)
        {
            var bookInDb = _libraryContext.Books
                .Where(x => x.BookId == bookForUpdate.BookId)
                .Include(x => x.Authors)
                .Include(x => x.Genres)
                .FirstOrDefault();

            if (bookInDb == null)
            {
                return;
            }

            _libraryContext.Entry(bookInDb).CurrentValues.SetValues(bookForUpdate);

            bookInDb.Authors = GetAuthorsForUpdate(bookInDb, bookForUpdate);
            bookInDb.Genres = GetGenresForUpdate(bookInDb, bookForUpdate);

            _libraryContext.SaveChanges();
        }

        private List<Author> GetAuthorsForUpdate(Book bookInDb, Book bookForUpdate)
        {
            var newAuthors = new List<Author>();
            var authorsInDb = bookInDb.Authors.ToDictionary(x => x.AuthorId);

            foreach (var author in bookForUpdate.Authors)
            {
                authorsInDb.TryGetValue(author.AuthorId, out var existingAuthor);

                if (existingAuthor == null)
                {
                    _libraryContext.Authors.Attach(author);
                    newAuthors.Add(author);
                }
                else
                {
                    newAuthors.Add(existingAuthor);
                }
            }

            return newAuthors;
        }

        private List<Genre> GetGenresForUpdate(Book bookInDb, Book bookForUpdate)
        {
            var newGenres = new List<Genre>();
            var genresInDb = bookInDb.Genres.ToDictionary(x => x.GenreId);

            foreach (var genre in bookForUpdate.Genres)
            {
                genresInDb.TryGetValue(genre.GenreId, out var existingGenre);

                if (existingGenre == null)
                {
                    _libraryContext.Genres.Attach(genre);
                    newGenres.Add(genre);
                }
                else
                {
                    newGenres.Add(existingGenre);
                }
            }

            return newGenres;
        }
    }
}