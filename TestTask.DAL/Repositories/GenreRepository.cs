﻿using System.Collections.Generic;
using System.Linq;
using TestTask.DAL.EF;
using TestTask.DAL.Entities;
using TestTask.DAL.Interfaces;
using TestTask.DAL.Repositories.Queries;

namespace TestTask.DAL.Repositories
{
    public class GenreRepository : IGenreRepository
    {
        private readonly LibraryContext _libraryContext;

        public GenreRepository(LibraryContext libraryContext)
        {
            _libraryContext = libraryContext;
        }

        public int GetGenresTotalCount()
        {
            var genresCount = _libraryContext.Genres.Count();

            return genresCount;
        }

        public Genre GetGenreById(int genreId)
        {
            var genre = _libraryContext.Genres.FirstOrDefault(x => x.GenreId == genreId);

            return genre;
        }

        public List<Genre> GetGenres(int skip, int take, GetGenresQuery query = null)
        {
            var genres = _libraryContext.Genres.AsQueryable();

            if (!string.IsNullOrWhiteSpace(query?.GenreName))
            {
                genres = genres.Where(x => x.Name.Contains(query.GenreName));
            }

            var result = genres.OrderBy(x => x.GenreId).Skip(skip).Take(take).ToList();
            return result;
        }
    }
}