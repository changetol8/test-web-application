﻿namespace TestTask.DAL.Migrations
{
    using System.Data.Entity.Migrations;
    
    public partial class LibraryMigration3 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Authors", "Patronymic", c => c.String());
            AddColumn("dbo.Authors", "BirthDate", c => c.DateTime());
            AddColumn("dbo.Books", "CreationDate", c => c.DateTime());
            AddColumn("dbo.Books", "Text", c => c.String());
            DropColumn("dbo.Authors", "MiddleName");
            DropColumn("dbo.Authors", "BurnDate");
            DropColumn("dbo.Books", "DateWrite");
            DropColumn("dbo.Books", "Content");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Books", "Content", c => c.String());
            AddColumn("dbo.Books", "DateWrite", c => c.DateTime());
            AddColumn("dbo.Authors", "BurnDate", c => c.DateTime());
            AddColumn("dbo.Authors", "MiddleName", c => c.String());
            DropColumn("dbo.Books", "Text");
            DropColumn("dbo.Books", "CreationDate");
            DropColumn("dbo.Authors", "BirthDate");
            DropColumn("dbo.Authors", "Patronymic");
        }
    }
}
