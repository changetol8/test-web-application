﻿namespace TestTask.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class LibraryMigration4 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Genres", "Book_BookId", "dbo.Books");
            DropIndex("dbo.Genres", new[] { "Book_BookId" });
            CreateTable(
                "dbo.GenreBooks",
                c => new
                    {
                        Genre_GenreId = c.Int(nullable: false),
                        Book_BookId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.Genre_GenreId, t.Book_BookId })
                .ForeignKey("dbo.Genres", t => t.Genre_GenreId, cascadeDelete: true)
                .ForeignKey("dbo.Books", t => t.Book_BookId, cascadeDelete: true)
                .Index(t => t.Genre_GenreId)
                .Index(t => t.Book_BookId);
            
            DropColumn("dbo.Genres", "Book_BookId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Genres", "Book_BookId", c => c.Int());
            DropForeignKey("dbo.GenreBooks", "Book_BookId", "dbo.Books");
            DropForeignKey("dbo.GenreBooks", "Genre_GenreId", "dbo.Genres");
            DropIndex("dbo.GenreBooks", new[] { "Book_BookId" });
            DropIndex("dbo.GenreBooks", new[] { "Genre_GenreId" });
            DropTable("dbo.GenreBooks");
            CreateIndex("dbo.Genres", "Book_BookId");
            AddForeignKey("dbo.Genres", "Book_BookId", "dbo.Books", "BookId");
        }
    }
}
