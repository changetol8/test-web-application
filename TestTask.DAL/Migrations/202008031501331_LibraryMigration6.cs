﻿namespace TestTask.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class LibraryMigration6 : DbMigration
    {
        public override void Up()
        {
            DropIndex("dbo.Authors", "AuthorSurnameIndex");
            DropIndex("dbo.Authors", "AuthorNameIndex");
            DropIndex("dbo.Books", "BookNameIndex");
            DropIndex("dbo.Genres", "GenreNameIndex");
            AlterColumn("dbo.Authors", "Surname", c => c.String(maxLength: 50));
            AlterColumn("dbo.Authors", "Name", c => c.String(maxLength: 50));
            AlterColumn("dbo.Books", "Name", c => c.String(maxLength: 50));
            AlterColumn("dbo.Genres", "Name", c => c.String(maxLength: 50));
            CreateIndex("dbo.Authors", "Surname");
            CreateIndex("dbo.Authors", "Name");
            CreateIndex("dbo.Books", "Name");
            CreateIndex("dbo.Genres", "Name");
        }
        
        public override void Down()
        {
            DropIndex("dbo.Genres", new[] { "Name" });
            DropIndex("dbo.Books", new[] { "Name" });
            DropIndex("dbo.Authors", new[] { "Name" });
            DropIndex("dbo.Authors", new[] { "Surname" });
            AlterColumn("dbo.Genres", "Name", c => c.String(maxLength: 50, unicode: false));
            AlterColumn("dbo.Books", "Name", c => c.String(maxLength: 50, unicode: false));
            AlterColumn("dbo.Authors", "Name", c => c.String(maxLength: 50, unicode: false));
            AlterColumn("dbo.Authors", "Surname", c => c.String(maxLength: 50, unicode: false));
            CreateIndex("dbo.Genres", "Name", name: "GenreNameIndex");
            CreateIndex("dbo.Books", "Name", name: "BookNameIndex");
            CreateIndex("dbo.Authors", "Name", name: "AuthorNameIndex");
            CreateIndex("dbo.Authors", "Surname", name: "AuthorSurnameIndex");
        }
    }
}
