﻿namespace TestTask.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class LibraryMigration31 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.GenreBooks", "Genre_GenreId", "dbo.Genres");
            DropForeignKey("dbo.GenreBooks", "Book_BookId", "dbo.Books");
            DropIndex("dbo.GenreBooks", new[] { "Genre_GenreId" });
            DropIndex("dbo.GenreBooks", new[] { "Book_BookId" });
            AddColumn("dbo.Genres", "Book_BookId", c => c.Int());
            CreateIndex("dbo.Genres", "Book_BookId");
            AddForeignKey("dbo.Genres", "Book_BookId", "dbo.Books", "BookId");
            DropTable("dbo.GenreBooks");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.GenreBooks",
                c => new
                    {
                        Genre_GenreId = c.Int(nullable: false),
                        Book_BookId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.Genre_GenreId, t.Book_BookId });
            
            DropForeignKey("dbo.Genres", "Book_BookId", "dbo.Books");
            DropIndex("dbo.Genres", new[] { "Book_BookId" });
            DropColumn("dbo.Genres", "Book_BookId");
            CreateIndex("dbo.GenreBooks", "Book_BookId");
            CreateIndex("dbo.GenreBooks", "Genre_GenreId");
            AddForeignKey("dbo.GenreBooks", "Book_BookId", "dbo.Books", "BookId", cascadeDelete: true);
            AddForeignKey("dbo.GenreBooks", "Genre_GenreId", "dbo.Genres", "GenreId", cascadeDelete: true);
        }
    }
}
