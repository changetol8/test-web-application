﻿namespace TestTask.DAL.Migrations
{
    using System.Data.Entity.Migrations;
    
    public partial class LibraryMigration2 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Authors", "BurnDate", c => c.DateTime());
            AlterColumn("dbo.Books", "DateWrite", c => c.DateTime());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Books", "DateWrite", c => c.DateTime(nullable: false));
            AlterColumn("dbo.Authors", "BurnDate", c => c.DateTime(nullable: false));
        }
    }
}
