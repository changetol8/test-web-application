﻿using System.Data;
using System.Data.SqlClient;

namespace TestTask.DAL.SqlCopy
{
    public class SqlBulkCopyFacade
    {
        private readonly string _connectionString;

        public SqlBulkCopyFacade(string connectionString)
        {
            _connectionString = connectionString;
        }

        public void FillDatabase(DataTable dataTable)
        {
            using (var sqlBulk = new SqlBulkCopy(_connectionString))
            {
                sqlBulk.DestinationTableName = dataTable.TableName;

                foreach (var column in dataTable.Columns)
                {
                    sqlBulk.ColumnMappings.Add(column.ToString(), column.ToString());
                }

                sqlBulk.WriteToServer(dataTable);
            }
        }
    }
}